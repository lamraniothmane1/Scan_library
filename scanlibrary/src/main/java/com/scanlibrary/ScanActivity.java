package com.scanlibrary;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Lamrani on 18/01/2018.
 */
public class ScanActivity extends Activity implements SurfaceHolder.Callback, IScanner  {

    //global variables
    private SurfaceView cameraView;
    private SurfaceHolder holder,holderTransparent;
    private Camera camera;
    private int  deviceHeight,deviceWidth;
    public static ImageView btn_load_image, btn_capture_image;
    private Camera.Parameters parameters;
    private static int RESULT_LOAD_IMG = 1;
    public static Fragment fragment_help_camera;
    private int REQUEST_CODE = 99;


    private Uri uri_original;
    private String result_CMC7;

    private int action;


    private final int ACTION_TAKE_PICTURE = 1;
    private final int ACTION_VALIDATE_PICTURE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.scan_layout);

        setContentView(R.layout.activity_camera);

        // full screen activity
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        init();

        // btn load image
        btn_load_image.setOnClickListener(new LoadFileClickListener());

        // btn capture image
        btn_capture_image.setOnClickListener(new CaptureImageClickListener());
    }

    private void init() {
        cameraView = (SurfaceView)findViewById(R.id.camera_view);

        btn_load_image = (ImageView) findViewById(R.id.btn_load_file);
        btn_capture_image = (ImageView) findViewById(R.id.btn_camera_capture);

        init_cameraView();

        // Auto focus configuration
        if(camera != null){
            cameraView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    camera.autoFocus(null);
                    return false;
                }
            });
        }
    }

    protected int getPreferenceContent() {
        return getIntent().getIntExtra(ScanConstants.OPEN_INTENT_PREFERENCE, 0);
    }

    @Override
    public void onBackPressed() {
        android.app.Fragment fragment = getFragmentManager().findFragmentById(R.id.content);

        if(fragment instanceof ScanFragment){
            Intent intent = new Intent(ScanActivity.this, ScanActivity.class);
            startActivity(intent);
            finish();
        }
        else{
            super.onBackPressed();
        }

    }

    @Override
    public void onBitmapSelect(Uri uri) {
        ScanFragment fragment = new ScanFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(ScanConstants.SELECTED_BITMAP, uri);
        fragment.setArguments(bundle);
        android.app.FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.content, fragment);
        fragmentTransaction.addToBackStack(ScanFragment.class.toString());
        fragmentTransaction.commit();
    }

    @Override
    public void onScanFinish(Uri uri) {
        ResultFragment fragment = new ResultFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(ScanConstants.SCANNED_RESULT, uri);
        fragment.setArguments(bundle);
        android.app.FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.content, fragment);
        fragmentTransaction.addToBackStack(ResultFragment.class.toString());
        fragmentTransaction.commit();
    }

    @Override
    public void onTrimMemory(int level) {
        switch (level) {
            case ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN:
                /*
                   Release any UI objects that currently hold memory.

                   The user interface has moved to the background.
                */
                break;
            case ComponentCallbacks2.TRIM_MEMORY_RUNNING_MODERATE:
            case ComponentCallbacks2.TRIM_MEMORY_RUNNING_LOW:
            case ComponentCallbacks2.TRIM_MEMORY_RUNNING_CRITICAL:
                /*
                   Release any memory that your app doesn't need to run.

                   The device is running low on memory while the app is running.
                   The event raised indicates the severity of the memory-related event.
                   If the event is TRIM_MEMORY_RUNNING_CRITICAL, then the system will
                   begin killing background processes.
                */
                break;
            case ComponentCallbacks2.TRIM_MEMORY_BACKGROUND:
            case ComponentCallbacks2.TRIM_MEMORY_MODERATE:
            case ComponentCallbacks2.TRIM_MEMORY_COMPLETE:
                /*
                   Release as much memory as the process can.

                   The app is on the LRU list and the system is running low on memory.
                   The event raised indicates where the app sits within the LRU list.
                   If the event is TRIM_MEMORY_COMPLETE, the process will be one of
                   the first to be terminated.
                */
                new AlertDialog.Builder(this)
                        .setTitle(R.string.low_memory)
                        .setMessage(R.string.low_memory_message)
                        .create()
                        .show();
                break;
            default:
                /*
                  Release any non-critical data structures.

                  The app received an unrecognized memory level value
                  from the system. Treat this as a generic low-memory message.
                */
                break;
        }
    }


    private File getOutputMediaFile() throws IOException {

        // Create a media file name
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "MyCameraApp");
        //File mediaStorageDir =  Environment.getExternalStorageDirectory();
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "IMG_" + timeStamp + ".jpg");

        Toast.makeText(this, "File saved", Toast.LENGTH_SHORT).show();

        return mediaFile;
    }



    private File getDir() {
        File sdDir = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        return new File(sdDir, "CameraAPIDemo");
    }



    /**
     * Initialize holders for camera view and draw help rectangle
     */
    private void init_cameraView() {
        holder = cameraView.getHolder();

        holder.addCallback((SurfaceHolder.Callback) this);

        //holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        cameraView.setSecure(true);

        //getting the device heigth and width

        deviceWidth = Resources.getSystem().getDisplayMetrics().widthPixels;;

        deviceHeight = Resources.getSystem().getDisplayMetrics().heightPixels;
    }




    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        cameraPreviewConfiguration();
    }

    private void cameraPreviewConfiguration() {
        try {
            //open a camera
            camera = Camera.open();

        }
        catch (Exception e) {
            Log.i("Exception", e.toString());
            return;
        }

        /* Set Auto focus */
        parameters = camera.getParameters();
        List<String> focusModes = parameters.getSupportedFocusModes();
        if(focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)){
            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        } else
        if(focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO)){
            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
        }



        /* Set orientation of the camera */
        android.hardware.Camera.CameraInfo info =
                new android.hardware.Camera.CameraInfo();

        android.hardware.Camera.getCameraInfo(0, info);

        int rotation = getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;

        switch (rotation) {
            case Surface.ROTATION_0: degrees = 0; break;
            case Surface.ROTATION_90: degrees = 90; break;
            case Surface.ROTATION_180: degrees = 180; break;
            case Surface.ROTATION_270: degrees = 270; break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        camera.setDisplayOrientation(result);

        parameters.setRotation(result);

        camera.setParameters(parameters);

        try {

            camera.setPreviewDisplay(holder);

            camera.startPreview();

        }

        catch (Exception e) {



            return;

        }

    }


    @Override
    protected void onDestroy() {

        super.onDestroy();

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        refreshCamera(); //call method for refress camera
    }

    public void refreshCamera() {
        if (holder.getSurface() == null) {
            return;
        }
        try {
            camera.stopPreview();
            camera.setPreviewDisplay(holder);

            cameraPreviewConfiguration();

            camera.startPreview();
        }

        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        //for release a camera
        if(camera != null){
            camera.release();
        }
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri uri = data.getExtras().getParcelable(ScanConstants.SCANNED_RESULT);
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                getContentResolver().delete(uri, null, null);
                //scannedImageView.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            // When an Image is picked
            if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
                // Get the Image from data
                Uri selectedImage = data.getData();
                onBitmapSelect(selectedImage);

            } else {
                Toast.makeText(this, "You haven't picked Image", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
        }
    }



    public void oldBitmapSelect(Uri uri) {
        ScanFragment fragment = new ScanFragment();
        Bundle bundle = new Bundle();

        bundle.putParcelable(ScanConstants.SELECTED_BITMAP,uri);
        fragment.setArguments(bundle);
        android.app.FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content, fragment);
        //fragmentTransaction.addToBackStack(ScanFragment.class.toString());
        fragmentTransaction.commit();

        uri_original = uri;
        action = ACTION_TAKE_PICTURE;
    }

    public void generateNoteOnSD(Context context, String sFileName, String sBody) {
        try {
            File root = new File(Environment.getExternalStorageDirectory(), "Notes");
            if (!root.exists()) {
                root.mkdirs();
            }
            File gpxfile = new File(root, sFileName);
            FileWriter writer = new FileWriter(gpxfile);
            writer.append(sBody);
            writer.flush();
            writer.close();
            //Toast.makeText(context, "Saved", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void oldScanFinish(final Uri uri) {

        Log.i("It works !!!!", uri.toString());

        // to show the image
        final ResultFragment fragment = new ResultFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(ScanConstants.SCANNED_RESULT, uri);
        bundle.putParcelable(ScanConstants.ORIGINAL, uri_original);
        fragment.setArguments(bundle);
        android.app.FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(com.scanlibrary.R.id.content, fragment);
        //fragmentTransaction.addToBackStack(ResultFragment.class.toString());
        fragmentTransaction.commit();

        action = ACTION_VALIDATE_PICTURE;

        /*try {
            Bitmap bitmapResult = Utils.getBitmap(this, uri);
            Log.i("Base 64 cropped Image", Utils.encodeTobase64(bitmapResult));

            String base64Image_string = Utils.encodeTobase64(bitmapResult);

            //generateNoteOnSD(getApplicationContext(), "caronae", Utils.encodeTobase64(bitmapResult));

            ScanController scanController = new ScanController();

            Call<String> call_scan = scanController.getCM7Result( base64Image_string);

            call_scan.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {

                    Log.i("Retrofit", "Success !!");

                    if(response.body() != null){
                        result_CMC7 = response.body();

                        Log.i("CMC7 result works !!", response.body());
                    }

                    fragment.setCMC7(result_CMC7);

                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.i("Retrofit Call API", "Failure");
                    if(t.getMessage() != null){
                        Log.i("Caused by", t.getMessage());
                    }

                    fragment.setCMC7(result_CMC7);
                }
            });


        } catch (IOException e) {
            e.printStackTrace();
        }*/

    }






    // ***************** On back pressed listener *******************
  /*
    @Override
    public void onBackPressed() {
      // on back listener do back
        switch (action){
            case 0: {
                Intent intent = new Intent(CameraActivity.this, ScanActivity.class);
                startActivity(intent);
                finish();
                break;
            }

            case ACTION_TAKE_PICTURE : {
                Intent intent = new Intent(CameraActivity.this, CameraActivity.class);
                startActivity(intent);
                finish();
                break;
            }
            case ACTION_VALIDATE_PICTURE :{
                onBitmapSelect(uri_original);
            }
            default:{
                Intent intent = new Intent(CameraActivity.this, ScanActivity.class);
                startActivity(intent);
                finish();
            }
        }

    }
    */



    private class LoadFileClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            // Create intent to Open Image applications like Gallery, Google Photos
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            // Start the Intent
            startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
        }
    }

    private class CaptureImageClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            Camera.PictureCallback mPicture = new Camera.PictureCallback() {
                @Override
                public void onPictureTaken(byte[] data, Camera camera) {
                    File pictureFile = null;
                    try {
                        pictureFile = getOutputMediaFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (pictureFile == null) {
                        return;
                    }
                    try {
                        FileOutputStream fos = new FileOutputStream(pictureFile);
                        fos.write(data);
                        fos.close();

                        Uri uri = Uri.fromFile(pictureFile);

                        onBitmapSelect(uri);

                    } catch (FileNotFoundException e) {

                    } catch (IOException e) {

                    }
                }
            };

            camera.takePicture(null, null, mPicture);
        }
    }


    public static native Bitmap getScannedBitmap(Bitmap bitmap, float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4);

    public static native Bitmap getGrayBitmap(Bitmap bitmap);

    public static native Bitmap getMagicColorBitmap(Bitmap bitmap);

    public static native Bitmap getBWBitmap(Bitmap bitmap);

    public static native float[] getPoints(Bitmap bitmap);

    static {
        System.loadLibrary("opencv_java3");
        System.loadLibrary("Scanner");
    }
}