package com.scanlibrary;

import android.net.Uri;

/**
 * Created by Lamrani on 18/01/2018.
 */
public interface IScanner {

    void onBitmapSelect(Uri uri);

    void onScanFinish(Uri uri);
}
